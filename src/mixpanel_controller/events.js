


const mixpanelEvents = {

    // the function executes after user succesfull registration
    completeRegistration: function(mixpanel, user_id, event_data) {

      mixpanel.alias(user_id)

      // !!!!!!!!!!!!!!!!!!!!!
      //register all super props when user register and each time when props value changes. For example - user changed plan_ we need re-register super props. It could be outside the events
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "Plan price": event_data.plan_price,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
        "Team_size": event_data.team_size,
      })

      // fill user properties
      mixpanel.track("Complete registration")
      mixpanel.people.set({
        '$name': event_data.name,
        '$email': event_data.email,
        'Registration date': event_data.reg_date,
        'Registration method': event_data.reg_method,
        'Plan name': event_data.plan_name,
        'Plan type': event_data.plan_type,
        'Plan price': event_data.plan_type,
        'UTM Source': event_data.utm_source,
        'UTM Content': event_data.utm_content,
        'UTM Medium': event_data.utm_medium,
        'UTM Term': event_data.utm_term,
      })

    },

    // the function executes after user succesfull login
    login: function(mixpanel, user_id, event_data) {

      mixpanel.identify(user_id)
      mixpanel.track("Login", {
        "Login method": event_data.login_method
      })
    },

    // the function executes after user view any page of the app or any section within page (pop up, tab or any view which appear within parent view)
    view_page_section: function (mixpanel, event_data) {
      mixpanel.track("View page/section", {
        "Page name": event_data.page_name,
        "Section name": event_data.section_name,
      })
    },

    // the function executes after user succesfully submit support ticket
    support_ticket_submitted: function (mixpanel) {
      mixpanel.track("Support ticket submitted")
      mixpanel.people.increment("Times need support")
    },

    // the function executes after user succesfully change plan
    change_plan: function (mixpanel, event_data) {
      mixpanel.track("Change plan", {
        'Old plan name': event_data.old_plan_name,
        'Old plan type': event_data.old_plan_type,
        'Old plan price': event_data.old_plan_price,
        'Plan name': event_data.plan_name,
        'Plan type': event_data.plan_type,
        'Plan price': event_data.plan_price,
        'Plan change': event_data.plan_change,
      })
      mixpanel.people.set(  {
        'Plan name': event_data.plan_name,
        'Plan type': event_data.plan_type,
        'Plan price': event_data.plan_price,

      })
      mixpanel.people.increment({
        'Times plan upgrades': 1,
        'Times plan downgrades': 1
      })

    },

    // the function executes after user succesfully cancel plan
    cancel_plan: function (mixpanel, event_data) {
      mixpanel.track("Cancel plan", {
        'Cancellation reason': event_data.cancellation_reason,
        'Old plan name': event_data.old_plan_name,
        'Old plan type': event_data.old_plan_type,
        'Old plan price': event_data.old_plan_price,
        'Plan name': event_data.plan_name,
        'Plan type': event_data.plan_type,
        'Plan price': event_data.plan_price,
        'Plan change': event_data.plan_change,
      })
      mixpanel.people.set(  {
        'Plan name': event_data.plan_name,
        'Plan type': event_data.plan_type,
      })
    },

    // the function executes after user changed payment method
    change_payment_method: function (mixpanel, event_data) {
      mixpanel.track("Change payment method", {
        "Old payment method": event_data.old_payment_method,
        "Payment method": event_data.payment_method
      })
      mixpanel.people.set(  {
        "Payment method": event_data.payment_method
      })
    },

    // the function executes after money transaction
    transaction: function (mixpanel, event_data) {
      mixpanel.people.track_charge(event_data.amount);
      mixpanel.people.increment({
        "Transactions times": 1,
        'Transactions': event_data.amount,
      })
    },

    //data integrations block

    // the function executes after user succesfull create data integration
    add_data_integration: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Add data integration ", {
        "Data integration ID": event_data.data_integration_id,
        "Data integration type": event_data.data_integration_type,
        "Time finish": event_data.time_finish,
      })
      mixpanel.people.append({
        "Data integrations": event_data.data_integration_id
      })
      mixpanel.people.increment("Times create data integration")

    },

    // the function executes after user succesfull edit data integration
    edit_data_integration: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Edit data integration", {
        "Data integration ID": event_data.data_integration_id,
        "Data integration type": event_data.data_integration_type,
        "Time finish": event_data.time_finish,
        'Edit connection settings': event_data.edit_connection_settings,
        'Edit attributes': event_data.edit_attributes,
      })
      mixpanel.people.increment("Times edit data integration")

    },

    // the function executes after user succesfull add user to data integration
    add_user_to_data_integration: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Add user to data integration", {
        "Data integration ID": event_data.dashboard_id,
        "Data integration type": event_data.data_integration_type,
        "Added user role": event_data.added_user_role
      })
      mixpanel.people.increment("Times add user to data integration")

    },

    // the function executes after user succesfull delete data integration
    delete_data_integration: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Delete data integration", {
        "Data integration ID": event_data.data_integration_id,
        "Data integration type": event_data.data_integration_type,
        "Export type": event_data.export_type
      })
      mixpanel.people.increment("Times delete data integration")
      mixpanel.people.remove({
        "Data integrations": event_data.data_integration_id
      })
    },




    //reports block

    // the function executes after user succesfull create report
    create_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Create report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
        "Time finish": event_data.time_finish,
        "Data integration IDs": event_data.data_integration_ids,
        "Report template": event_data.report_template,
        "Report diagrams": event_data.report_diagrams,
        "Report categories": event_data.report_categories,
        "Report values": event_data.report_values,
        "Edit filters": event_data.edit_filters,
        "Edit format": event_data.edit_format,
        'Save report': event_data.save_report,
      })
      mixpanel.people.append({
        "Reports": event_data.report_id
      })

      if (event_data.save_report === false) {
        mixpanel.people.increment("Unsaved created report")
      } else {
        mixpanel.people.increment("Times create report")
      }

    },

    // the function executes after user succesfull edit report
    edit_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Edit report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
        "Time finish": event_data.time_finish,
        'Old data integration IDs': event_data.old_data_integration_ids,
        'Data integration IDs': event_data.data_integration_ids,
        'Old report diagram': event_data.old_report_diagram,
        'Report diagram': event_data.report_diagram,
        'Edit filters': event_data.edit_data_filters,
        'Edit format': event_data.edit_data_format,
        'Save report changes': event_data.save_report_changes,
      })

      if (event_data.save_report_changes === false) {
        mixpanel.people.increment("Unsaved report changes")
      } else {
        mixpanel.people.increment("Times edit report")
      }

    },

    // the function executes after user succesfull share report
    share_report: function (mixpanel, event_data) {
      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Share report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
      })

      mixpanel.people.increment("Times share report")
    },

    // the function executes after user succesfull add user report
    add_user_to_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Add user to report", {

        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
        "Added user role": event_data.added_user_role
      })

      mixpanel.people.increment("Times add user to report")

    },

    // the function executes after user view report
    view_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("View report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
        "Time finish": event_data.time_finish,
        "Use query": event_data.use_query
      })

      mixpanel.people.increment("Times view report")

    },

    // the function executes after user succesfull export report
    export_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Export report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
        "Export type": event_data.export_type
      })

      mixpanel.people.increment("Times export report")
    },

    // the function executes after user succesfull delete report
    delete_report: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Delete report", {
        "Report ID": event_data.report_id,
        "Report name": event_data.report_name,
      })

      mixpanel.people.increment("Times delete report")

      mixpanel.people.remove({
        "Reports": event_data.report_id
      })
    },

    //dashboards block

    // the function executes after user succesfull create dashboard
    create_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Create dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
        "Time finish": event_data.time_finish,
        'Save dashboard': event_data.save_dashboard,
        'Dashboard template': event_data.dashboard_template,
        'Use graphs': event_data.use_graph,
        'Use reports': event_data.use_reports,
        'Use text': event_data.use_text,
        'Use img': event_data.use_img,
        'Use html': event_data.use_html,
      })

      mixpanel.people.append({
        "Dashboards": event_data.dashboard_id
      })

      if (event_data.save_report === false) {
        mixpanel.people.increment("Unsaved created dashboard")
      } else {
        mixpanel.people.increment("Times create dashboard")
      }

    },

    // the function executes after user succesfully edited dashboard
    edit_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Edit dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
        "Time finish": event_data.time_finish,
        'Save dashboard changes': event_data.save_dashboard_changes,
        'Dashboard template': event_data.dashboard_template,
        'Edit graphs': event_data.edit_graph,
        'Edit reports': event_data.edit_reports,
        'Edit text': event_data.edit_text,
        'Edit img': event_data.edit_img,
        'Edit html': event_data.edit_html,
      })

      if (event_data.save_report_changes === false) {
        mixpanel.people.increment("Unsaved dashboard changes")
      } else {
        mixpanel.people.increment("Times edit dashboard")
      }

    },

    // the function executes after user succesfully shared dashboard
    share_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Share dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
      })

      mixpanel.people.increment("Times share dashboard")
    },

    // the function executes after user succesfully add user to dashboard
    add_user_to_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Add user to dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
        "Added user role": event_data.added_user_role
      })

      mixpanel.people.increment("Times add user to dashboard")

    },

    // the function executes after user view dashboard
    view_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("View dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
        "Time finish": event_data.time_finish,
        "Use query": event_data.use_query
      })

      mixpanel.people.increment("Times view dashboard")

    },

    // the function executes after user succesfully exported dashboard
    export_dashboard: function (mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Export dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
        "Export type": event_data.export_type
      })

      mixpanel.people.increment("Times export dashboard")
    },

    // the function executes after user succesfully deleted dashboard
    delete_dashboard: function(mixpanel, event_data) {

      //register all super props
      mixpanel.register({
        "Plan name": event_data.plan_name,
        "Plan type": event_data.plan_type,
        "User role": event_data.user_role,
        "Team_id": event_data.team_id,
        "Team_name": event_data.team_name,
      })

      mixpanel.track("Export dashboard", {
        "Dashboard ID": event_data.dashboard_id,
        "Dashboard name": event_data.dashboard_name,
      })

      mixpanel.people.remove({
        "Dashboards": event_data.dashboard_id
      })

      mixpanel.people.increment("Times delete dashboard")
    }


}


module.exports = mixpanelEvents
