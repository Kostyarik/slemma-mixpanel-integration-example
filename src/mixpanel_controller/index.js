import Mixpanel from "mixpanel-browser"
import MixpanelEvents from './events.js'
import MIX_PANEL_CONFIG from './config.js'

// initialize mixpanel client configured to communicate over https
// better to push it global
const mixpanel = Mixpanel.init(MIX_PANEL_CONFIG.MIX_PANEL_TOKEN, {
  protocol: 'https',
  debug: true
});
const mixpanelEvents = new MixpanelEvents(mixpanel)


module.exports = mixpanelEvents;
