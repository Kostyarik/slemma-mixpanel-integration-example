const superProps = {

  "Page name": "",
  "Section name": "",
  "Plan name": "",
  "Plan type": "",
  "User role": "",
  "Team_id": "",
  "Team_name": "",
  "$time": "",
}

const mixpanelEvents = {


  // the function executes after user succesfull registration
  completeRegistration: function(mixpanel, user_id, user) {

    // fill user properties
    mixpanel.people.set(user_id, {
      '$name': user.name,
      '$email': user.email,
      'Company size': user.company_size,
      'Company name': user.company_name,
      'Registration date': user.reg_date,
      'Registration method': user.reg_method,
      'Plan name': user.plan_name,
      'Plan type': user.plan_type,
      'UTM Source': user.utm_source,
      'UTM Content': user.utm_content,
      'UTM Medium': user.utm_medium,
      'UTM Term': user.utm_term,
    })


    mixpanel.track("Complete registration", {
      distinct_id: user_id,

    })
  },

  // the function executes after user succesfull login
  login: function (mixpanel, user_id, event_data) {

    mixpanel.track("Login", {
      distinct_id: user_id,
      "Login method": event_data.login_method
    })
  },

  // the function executes after user view any page of the app or any section within page (pop up, tab or any view which appear within parent view)
  view_page_section: function (mixpanel, user_id, event_data) {
    mixpanel.track("View page/section", {
      distinct_id: user_id,
      // "Page name": event_data.page_name,
      // "Section name": event_data.page_section,
      // "Time start": event_data.time_start,
    })
  },

  // the function executes after user succesfull submit support ticket
  support_ticket_submitted: function (mixpanel, user_id, event_data) {

    mixpanel.track("Support ticket submitted", {
      distinct_id: user_id,
    })

    mixpanel.people.increment(user_id,"Times need support")
  },

  // the function executes after user succesfull change plan
  change_plan: function (mixpanel, user_id, event_data) {

    mixpanel.track("Change plan", {
      distinct_id: user_id,
      'Old plan name': event_data.old_plan_name,
      'Old plan type': event_data.old_plan_type,
      'Plan name': event_data.plan_name,
      'Plan type': event_data.plan_type,
      'Plan change': event_data.plan_change,
    })

    mixpanel.people.set(user_id,{
      'Plan name': event_data.plan_name,
      'Plan type': event_data.plan_type,
    })

    mixpanel.people.increment(user_id,{
      'Times plan upgrades': 1,
      'Times plan downgrades': 1
    })

  },

  // the function executes after user succesfull cancel plan
  cancel_plan: function (mixpanel, user_id, event_data) {

    mixpanel.track("Cancel plan", {
      distinct_id: user_id,
      'Cancellation type': event_data.cancellation_type,
      'Cancellation reason': event_data.cancellation_reason,
      'Old plan name': event_data.old_plan_name,
      'Old plan type': event_data.old_plan_type,
      'Plan name': event_data.plan_name,
      'Plan type': event_data.plan_type,
      'Plan change': event_data.plan_change,
    })

    mixpanel.people.set(user_id,{
      'Plan name': event_data.plan_name,
      'Plan type': event_data.plan_type,
    })
  },

  // the function executes after money transaction
  change_payment_method: function (mixpanel, user_id, event_data) {
    mixpanel.track(user_id,{
      "Old payment method": event_data.old_payment_method,
      "Payment method": event_data.payment_method
    })
    mixpanel.people.set(user_id,{
      "Payment method": event_data.payment_method
    })
  },

  // the function executes after user change payment method
  transaction: function (mixpanel, user_id, event_data) {
    mixpanel.people.track_charge(
      user_id,
      event_data.amount,
    );

    mixpanel.people.increment(user_id,{
      "Transactions times": 1,
      'Transactions': event_data.amount,
    })
  },


  //data integrations block

  // the function executes after user succesfull create data integration
  add_data_inregration: function (mixpanel, user_id, event_data) {

    mixpanel.track("Add data integration ", {
      distinct_id: user_id,
      "Data integration ID": event_data.data_integration_id,
      "Data integration type": event_data.data_integration_type,
      "Time finish": event_data.time_finish,
    })

    mixpanel.people.append(user_id,{
      "Data integrations": event_data.data_integrations
    })

    mixpanel.people.increment(user_id, "Times create data integration")

  },

  // the function executes after user succesfull edit data integration
  edit_data_inregration: function (mixpanel, user_id, event_data) {

    mixpanel.track("Edit data integration", {
      distinct_id: user_id,
      "Data integration ID": event_data.data_integration_id,
      "Time finish": event_data.time_finish,
      'Edit connection settings': event_data.edit_connection_settings,
      'Edit attributes': event_data.edit_attributes,
    })

    mixpanel.people.increment(user_id, "Times edit data integration")

  },

  // the function executes after user succesfull share data integration
  add_user_to_data_integration: function (mixpanel, user_id, event_data) {

    mixpanel.track("Add user to data integration", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "User Type": event_data.user_type
    })

    mixpanel.people.increment(user_id,"Times add user to data integration")

  },

  // the function executes after user succesfull export data integration
  delete_data_integration: function (mixpanel, user_id, event_data) {

    mixpanel.track("Delete data integration", {
      distinct_id: user_id,
      "Data integration ID": event_data.data_integration_id,
      "Data integration type": event_data.data_integration_type,
      "Export type": event_data.export_type
    })

    mixpanel.people.increment(user_id,"Times delete data integration")
  },


  //reports block

  // the function executes after user succesfull create report
  create_report: function (mixpanel, user_id, event_data) {

    mixpanel.track("Create report", {
      distinct_id: user_id,
      "Report ID": event_data.report_id,
      "Report name": event_data.report_name,
      "Time finish": event_data.time_finish,
      "Data integration IDs": event_data.data_integration_ids,
      "Report template": event_data.report_template,
      "Report diagrams": event_data.report_diagrams,
      "Report categories": event_data.report_categories,
      "Report values": event_data.report_values,
      "Edit filters": event_data.edit_filters,
      "Edit format": event_data.edit_format,
      'Save report': event_data.save_report,

    })
r
    mixpanel.people.append(user_id,{
      "Reports": event_data.data_integrations
    })


    if (event_data.save_report === false) {
      mixpanel.people.increment(user_id, "Unsaved created report")
    } else {
      mixpanel.people.increment(user_id, "Times create report")
    }


  },

  // the function executes after user succesfull edit report
  edit_report: function (mixpanel, user_id, event_data) {

    mixpanel.track("Edit report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Time start": event_data.time_start,
      "Time finish": event_data.time_finish,
      'Save dashboard': event_data.save_dashboard,
      'Data integrations': event_data.data_integrations,
      'Edit data filters': event_data.edit_data_filters,
      'Edit data layout': event_data.edit_data_layout,
      'Edit data format': event_data.edit_data_format,
    })

    mixpanel.people.increment(user_id, "Times edit report")

  },

  // the function executes after user succesfull share report
  share_report: function (mixpanel, user_id, event_data) {
    mixpanel.track("Share report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
    })

    mixpanel.people.increment(user_id,"Times share report")
  },

  // the function executes after user succesfull share report
  add_user_to_report: function (mixpanel, user_id, user, event_data) {

    mixpanel.track("Add user to report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "User Type": event_data.user_type
    })

    mixpanel.people.increment(user_id,"Times add user to report")

  },

  // the function executes after user view report
  view_report: function (mixpanel, user_id, event_data) {

    mixpanel.track("View report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Time Start": event_data.time_start,
      "Time Finish": event_data.time_finish,
      "Use query": event_data.use_query
    })

    mixpanel.people.increment(user_id, "Times view report")

  },

  // the function executes after user succesfull export report
  export_report: function (mixpanel, user_id, event_data) {

    mixpanel.track("Export report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Export type": event_data.export_type
    })

    mixpanel.people.increment(user_id,"Times export report")
  },

  // the function executes after user succesfull export report
  delete_report: function (mixpanel, user_id, event_data) {

    mixpanel.track("Delete report", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Export type": event_data.export_type
    })

    mixpanel.people.increment(user_id,"Times delete report")
  },


  //dashboards block

  // the function executes after user succesfull create dashboard
  create_dashboard: function (mixpanel, user_id, event_data) {

    mixpanel.track("Create dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Time start": event_data.time_start,
      "Time finish": event_data.time_finish,
      'Save dashboard': event_data.save_dashboard,
      'Dashboard template': event_data.dashboard_template,
      'Data integrations': event_data.data_integrations,
      'Use data filters': event_data.use_data_filters,
      'Use data layout': event_data.use_data_layout,
      'Use data format': event_data.use_data_format,
    })

    mixpanel.people.increment(user_id, "Times create dashboard")

  },

  // the function executes after user succesfull edit dashboard
  edit_dashboard: function (mixpanel, user_id, event_data) {

    mixpanel.track("Edit dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Time start": event_data.time_start,
      "Time finish": event_data.time_finish,
      'Save dashboard': event_data.save_dashboard,
      'Data integrations': event_data.data_integrations,
      'Edit data filters': event_data.edit_data_filters,
      'Edit data layout': event_data.edit_data_layout,
      'Edit data format': event_data.edit_data_format,
    })

    mixpanel.people.increment(user_id, "Times edit dashboard")

  },

  // the function executes after user succesfull share dashboard
  share_dashboard: function (mixpanel, user_id, event_data) {
    mixpanel.track("Share dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
    })

    mixpanel.people.increment(user_id,"Times share dashboard")
  },

  // the function executes after user succesfull share dashboard
  add_user_to_dashboard: function (mixpanel, user_id, user, event_data) {

    mixpanel.track("Add user to dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "User Type": event_data.user_type
    })

    mixpanel.people.increment(user_id,"Times add user to dashboard")

  },

  // the function executes after user view dashboard
  view_dashboard: function (mixpanel, user_id, event_data) {

    mixpanel.track("View dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Time Start": event_data.time_start,
      "Time Finish": event_data.time_finish,
      "Use query": event_data.use_query
    })

    mixpanel.people.increment(user_id, "Times view dashboard")

  },

  // the function executes after user succesfull export dashboard
  export_dashboard: function (mixpanel, user_id, event_data) {

    mixpanel.track("Export dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Export type": event_data.export_type
    })

    mixpanel.people.increment(user_id,"Times export dashboard")
  },

  // the function executes after user succesfull export dashboard
  delete_dashboard: function (mixpanel, user_id, event_data) {

    mixpanel.track("Export dashboard", {
      distinct_id: user_id,
      "Dashboard ID": event_data.dashboard_id,
      "Dashboard name": event_data.dashboard_name,
      "Export type": event_data.export_type
    })

    mixpanel.people.increment(user_id,"Times export dashboard")
  },


}


module.exports = mixpanelEvents
