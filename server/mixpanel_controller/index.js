const MIX_PANEL_CONFIG = require('../mixpanel_controller/config.js')

// grab the Mixpanel factory
var Mixpanel = require('mixpanel');

// initialize mixpanel client configured to communicate over https
var mixpanel = Mixpanel.init(MIX_PANEL_CONFIG.MIX_PANEL_TOKEN, {
  protocol: 'https',
  debug: true
});

module.exports = mixpanel
