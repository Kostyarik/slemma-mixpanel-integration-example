const express = require("express")
const mixpanel = require('../mixpanel_controller')
const mixpanelEvents = require('../mixpanel_controller/events.js')
const router = express.Router();


// test user id, in prod you will need to query real user id from database

const user_id = "testProfile"

router.post("/completeRegistration", (req, res, next) => {
  mixpanelEvents.completeRegistration(mixpanel, user_id, req.query)
});

router.post("/login", (req, res, next) => {
  mixpanelEvents.login(mixpanel, user_id, req.query)
});

router.post("/view_page_section", (req, res, next) => {
  mixpanelEvents.view_page_section(mixpanel, user_id, req.query)
});

router.post("/support_ticket_submitted", (req, res, next) => {
  mixpanelEvents.support_ticket_submitted(mixpanel, user_id, req.query)
});

router.post("/create_dashboard", (req, res, next) => {
  mixpanelEvents.create_dashboard(mixpanel, user_id, req.query)
});

router.post("/edit_dashboard", (req, res, next) => {
  mixpanelEvents.edit_dashboard(mixpanel, user_id, req.query)
});

router.post("/share_dashboard", (req, res, next) => {
  mixpanelEvents.share_dashboard(mixpanel, user_id, req.query)
});

router.post("/add_user_to_dashboard", (req, res, next) => {
  mixpanelEvents.add_user_to_dashboard(mixpanel, user_id, req.query)
});

router.post("/view_dashboard", (req, res, next) => {
  mixpanelEvents.view_dashboard(mixpanel, user_id, req.query)
});

router.post("/export_dashboard", (req, res, next) => {
  mixpanelEvents.export_dashboard(mixpanel, user_id, req.query)
});

router.post("/change_plan", (req, res, next) => {
  mixpanelEvents.change_plan(mixpanel, user_id, req.query)
});

router.post("/cancel_plan", (req, res, next) => {
  mixpanelEvents.cancel_plan(mixpanel, user_id, req.query)
});

router.post("/transaction", (req, res, next) => {
  mixpanelEvents.transaction(mixpanel, user_id, req.query)
});


module.exports = router;
